function counterFactory(){
    return [ 
        count = 10 ,

        function increment(count){
            count += 1
        },
        function decrement(count){
            count -= 1
        }
    ]

}

function limitFunctionCallCount(cb, n) {
    let c1 = 1
    return function fn(){
        if (c1 <= n) {
            c1 +=1
            return cb();
        }
        else 
            return null;
        }
    }

function cacheFunction(cb) {
    let cache = {}
    return function fn1() {
        let args = [...arguments].join(',')
        if ( args in cache){
            return cache[args]
        }
        else {
            cache[args] = cb(...arguments)
            return cache[args]
        }
    }
}
module.exports = { counterFactory, limitFunctionCallCount, cacheFunction }
