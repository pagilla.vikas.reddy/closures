const {counterFactory, limitFunctionCallCount, cacheFunction} = require('./closure');

const limitMethod = limitFunctionCallCount(() => "Vikas", 2);
console.log(limitMethod()); 
console.log(limitMethod()); 
console.log(limitMethod()); 
console.log(limitMethod()); 
console.log(limitMethod()); 

const cache = cacheFunction((x , y) => x + y);
console.log(cache(5,3));


